<?php

namespace App\Tests;

use App\Entity\User;
use DateTimeImmutable;
use PHPUnit\Framework\TestCase;

class UserUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $user = new User();
        $date = new DateTimeImmutable();

        $user
            ->setEmail('true@test.com')
            ->setFirstname('firstname')
            ->setLastname('lastname')
            ->setPassword('password')
            ->setCreatedAt($date);

        $this->assertTrue($user->getEmail() === 'true@test.com');
        $this->assertTrue($user->getFirstname() === 'firstname');
        $this->assertTrue($user->getLastname() === 'lastname');
        $this->assertTrue($user->getPassword() === 'password');
        $this->assertTrue($user->getCreatedAt() === $date);
    }

    public function testIsFalse(): void
    {
        $user = new User();

        $user
            ->setEmail('true@test.com')
            ->setFirstname('firstname')
            ->setLastname('lastname')
            ->setPassword('password')
            ->setCreatedAt(new DateTimeImmutable());

        $this->assertFalse($user->getEmail() === 'false@test.com');
        $this->assertFalse($user->getFirstname() === 'false');
        $this->assertFalse($user->getLastname() === 'false');
        $this->assertFalse($user->getPassword() === 'false');
        $this->assertFalse($user->getCreatedAt() === new DateTimeImmutable());
    }

    public function testIsEmpty(): void
    {
        $user = new User();

        $this->assertEmpty($user->getEmail());
        $this->assertEmpty($user->getFirstname());
        $this->assertEmpty($user->getLastname());
        $this->assertEmpty($user->getPassword());
        $this->assertEmpty($user->getCreatedAt());
    }
}
