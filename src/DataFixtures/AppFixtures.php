<?php

namespace App\DataFixtures;

use App\Entity\User;
use DateTimeImmutable;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

/**
 * @codeCoverageIgnore
 */
class AppFixtures extends Fixture
{
    private UserPasswordHasherInterface $hasher;

    public function __construct(UserPasswordHasherInterface $hasher)
    {
        $this->hasher = $hasher;
    }

    public function load(ObjectManager $manager): void
    {
        $user = new User();
        $password = $this->hasher->hashPassword($user, 'password');

        $user
            ->setEmail('admin@admin.fr')
            ->setCreatedAt(new DateTimeImmutable())
            ->setFirstname('admin')
            ->setLastname('admin')
            ->setRoles(['ROLE_ADMIN'])
            ->setPassword($password);

        $manager->persist($user);

        $manager->flush();
    }
}
